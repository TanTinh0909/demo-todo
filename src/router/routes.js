// single pages
import homePage from '../pages/ScreenHome.vue';
import loginPage from '../pages/ScreenLogin.vue';
// import notFoundPage from '../pages/NotFound.vue';


export const routes = [
  {
    path: '/login',
    components: {
      default: loginPage,
    },
  },
  {
    path: '/',
    components: {
      default: homePage,
    },
  },
];
